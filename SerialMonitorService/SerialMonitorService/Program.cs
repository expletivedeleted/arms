﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace SerialMonitorService
{
    class Program
    {
        static string ID;

        static string cs = @"server=localhost;userid=arms_admin;
            password=2xkznskRw5LL0otE;database=maindb;sslmode=none";

        static MySqlConnection conn = null;
        static void InitializeMySQL()
        {
            try
            {
            Console.Write("Enter server address (localhost): ");
                string sv = Console.ReadLine();

            if (sv.Length <= 0)
                sv = "localhost";

                cs = string.Format(@"server={0};userid=arms_admin;
            password=2xkznskRw5LL0otE;database=maindb;sslmode=none", sv);


            conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT VERSION()";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                string version = Convert.ToString(cmd.ExecuteScalar());
                Console.WriteLine("MySQL version : {0}", version);

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (conn != null)
                {
                    conn.Close();
                }

            }
        }

        static void InsertRecord(string n, string o)
        {
            try
            {
                string recordID = "";

                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT ID FROM attendance where UserID = '" + n + "' and DateOut is null and RoomID = '"+o+"';";
                MySqlCommand cmd = new MySqlCommand(stm, conn);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    recordID = reader.GetString(0);
                }

                conn.Close();

                if (recordID != "")
                {
                    stm = "UPDATE attendance SET DateOut = NOW() where ID = '" + recordID + "'"; 
                }
                else
                {
                    stm = "INSERT INTO attendance (RoomID, UserID, DateIn) values ('"+o+"','"+n+"', NOW())";
                }

                conn = new MySqlConnection(cs);
                conn.Open();
                cmd = new MySqlCommand(stm, conn);
                cmd.ExecuteScalar();

                Console.WriteLine(recordID == ""? "-login" : "-logout");
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (conn != null)
                {
                    conn.Close();
                }

            }
        }

        static SerialPort _serialPort;
        public static void Main()
        {
            InitializeMySQL();
            ID = "";

            Console.Write("Room ID: ");
            string roomNo = Console.ReadLine();

            Console.Write("Serial COM Port: ");
            string port = Console.ReadLine();

            _serialPort = new SerialPort();
            _serialPort.PortName = port;
            _serialPort.BaudRate = 57600;
            _serialPort.Open();

            Console.WriteLine("--READY--");
            while (true)
            {
                string a = _serialPort.ReadExisting();

                if (a.Length > 1)
                {
                    if (a.StartsWith("<START>") && a.EndsWith("<END>"))
                    {
                        string s = a.Substring(7, a.Length - 12);
                        ID = s.Split(',')[3];
                    }
                    Console.WriteLine("Found ID: " + ID);
                    InsertRecord(ID, roomNo);
                }
                Thread.Sleep(200);
            }
        }
    }
}