<?php
    session_start();
    if(!isset($_SESSION['login'])){ //if login in session is not set
        header("Location: login.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include("templates/inc_head.php"); ?>
    <title>ARMS | Home</title>
</head>
<body>
    <?php include("templates/header.php");?>

    <div class="container-fluid">
        <div class="row">
            <?php $_GET["tab"] = "users"; include("templates/sidebar.php");?>
            <div class="col-9">
                <div style="padding: 20px">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNewUserModal">Add New Faculty Account</button>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Full Name</th>
                            <th>Department</th>
                        </tr>
                    </thead>
                    <tbody id="tblRecords">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- MODAL -->

    <?php
        $_GET["modalID"]="addNewUserModal";
        $_GET["modalTitle"]="Add New Faculty Account";
        include("templates/modal_start.php");
    ?>

    <div class="form-group">
        <label for="txtUserID">User ID</label>
        <input type="text" class="form-control" name="txtUserID" id="txtUserID" placeholder="User ID">

        <label for="txtFullName">Full name</label>
        <input type="text" class="form-control" name="txtFullName" id="txtFullName" placeholder="Full Name">

        <label for="txtDepartment">Department</label>
        <input type="text" class="form-control" name="txtDepartment" id="txtDepartment" placeholder="Department">
    </div>

    <?php
        $_GET["modalOK"]="AddNewRecord();";
        include("templates/modal_end.php");?>
    
</body>

<?php include("templates/inc_foot.php"); ?>
<script src="js/script_user.js"></script>
</html>