<?php

    include("connection.php");

    $query = "select * from rooms;";

    if (!$result = mysqli_query($conn, $query)) {
        exit(mysqli_error($conn));
    }

    $data = "";
    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
        $oc = true;

        while($row = mysqli_fetch_assoc($result))
        {
            $id = $row['ID'];
            $oc = false;
            $f = "None";
            $query = "select * from attendance where RoomID='$id' and DateOut is null;";

            if (!$result = mysqli_query($conn, $query)) {
                exit(mysqli_error($conn));
            }
            
            while($row2 = mysqli_fetch_assoc($result))
            {
                $oc = true;
                $f = $row2['UserID'];
            }

            $data .= '<tr>
            <td>'.$row['ID'].'</td>
            <td>'.$row['Name'].'</td>
            <td>'.$f.'</td>
            <td style="color: '.($oc? "red" : "green").'">'.($oc? 'Occupied' : 'Not Occupied').'</td>
            </tr>';
        }
    }
    else
    {
        // records now found 
    }

    echo $data;
?>