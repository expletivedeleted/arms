<?php

    include("connection.php");

    $query = "select * from attendance;";

    if (!$result = mysqli_query($conn, $query)) {
        exit(mysqli_error($conn));
    }

    $data = "";
    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
        while($row = mysqli_fetch_assoc($result))
        {
            $data .= '<tr>
            <td>'.$row['UserID'].'</td>.
            <td>'.$row['RoomID'].'</td>.
            <td>'.($row['DateIn'] == "0000-00-00 00:00:00"? "" : $row['DateIn']).'</td>.
            <td>'.($row['DateOut'] == "0000-00-00 00:00:00"? "" : $row['DateOut']).'</td></tr>';
        }
    }
    else
    {
        // records now found 
    }

    echo $data;
?>