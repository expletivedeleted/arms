<?php

    include("connection.php");

    $query = "select * from users where Department<>'Admin' order by ID;";

    if (!$result = mysqli_query($conn, $query)) {
        exit(mysqli_error($conn));
    }

    $data = "";
    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
        $oc = true;

        while($row = mysqli_fetch_assoc($result))
        {
            $ID = $row['ID'];
            $FullName = $row['FullName'];
            $Department = $row['Department'];

            $data .= '<tr>
                <td>'.$ID.'</td>
                <td>'.$FullName.'</td>
                <td>'.$Department.'</td>
                <td>
                    <button type="button" class="btn btn-info-outline" onclick="EditRecord(\''.$ID.'\',\''.$FullName.'\',\''.$Department.'\');">Edit Details</button>
                </td>
            </tr>';
        }
    }
    else
    {
        // records now found 
    }

    echo $data;
?>