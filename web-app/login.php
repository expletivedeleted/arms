<?php
    session_start();
    if(isset($_SESSION['login'])){
        header("location: index.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include("templates/inc_head.php"); ?>
    <title>ARMS | Login</title>
</head>
<body>
    <?php include("templates/header.php");?>

    <div class="container">
        <div class="row">
                <div class="form-group">
                    <label for="txtUsername">Admin Login</label>
                    <input type="text" name="txtUsername" id="txtUsername" class="form-control" placeholder="Username">
                    <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="Password">
                    
                <button class="btn btn-primary" id="btnLogin">LOGIN</button>
                </div>
        </div>
    </div>
</body>

<?php include("templates/inc_foot.php"); ?>
</html>