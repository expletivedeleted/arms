<?php
    session_start();
    if(!isset($_SESSION['login'])){ //if login in session is not set
        header("Location: login.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include("templates/inc_head.php"); ?>
    <title>ARMS | Home</title>
</head>
<body>
    <?php include("templates/header.php");?>

    <div class="container-fluid">
        <div class="row">
            <?php $_GET["tab"] = ""; include("templates/sidebar.php");?>
            <div class="col-9">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Room ID</th>
                            <th>Time In</th>
                            <th>Time Out</th>
                        </tr>
                    </thead>
                    <tbody id="tblRecords">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

<?php include("templates/inc_foot.php"); ?>
<script src="js/script_index.js"></script>
</html>