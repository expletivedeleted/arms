<?php
    session_start();
    if(!isset($_SESSION['login'])){ //if login in session is not set
        header("Location: login.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include("templates/inc_head.php"); ?>
    <title>ARMS | Home</title>
</head>
<body>
    <?php include("templates/header.php");?>

    <div class="container-fluid">
        <div class="row">
            <?php $_GET["tab"] = "rooms"; include("templates/sidebar.php");?>
            <div class="col-9">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Room ID</th>
                            <th>Room Name</th>
                            <th>Current Faculty</th>
                            <th>Availability</th>
                        </tr>
                    </thead>
                    <tbody id="tblRecords">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

<?php include("templates/inc_foot.php"); ?>
<script src="js/script_room.js"></script>
</html>