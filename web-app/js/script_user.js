var currentUser = null;

$(document).ready(function () {
    loadRecords(null);
});


function loadRecords(f)
{
    currentUser = null;
    EDITMODE = false;
    $.get("backend/get_user_details.php", {
        
    }, function (data, status) {
        $("#tblRecords").html(data);
    });
}

function AddNewRecord()
{
    console.log("EDITMODE: " + EDITMODE);
    $.post("backend/add_new_user.php", {
        editmode: EDITMODE? 1 : 0,
        userid: $("#txtUserID").val(),
        fullname: $("#txtFullName").val(),
        department: $("#txtDepartment").val()
    }, function (data, status) {
        if (data.startsWith("ok"))
        {
            $('#addNewUserModal').modal('hide');
            loadRecords(null);
        }else{
            if (data.startsWith("validation"))
            {
                showModalError("Please input all fields");
            }else if (data.startsWith("id")){
                showModalError("User ID already used by other user");
            }
        }
    });
}

function EditRecord(id,fn,d)
{
    EDITMODE = true;
    $('#addNewUserModal').modal('show');
    $("#txtUserID").val(id);
    $("#txtUserID").attr('readonly', true);
    $("#txtFullName").val(fn);
    $("#txtDepartment").val(d);
}