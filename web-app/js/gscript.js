var AJAX_INTERVAL = 5000;
var EDITMODE = false;


$(document).ready(function () {

    $('.modal').on('hidden.bs.modal', function (e) {
        $(this)
        .find("input,textarea,select")
            .attr('readonly', false)
            .val('')
            .end()
        .find("input[type=checkbox], input[type=radio]")
            .attr('readonly', false)
            .prop("checked", "")
            .end()
        .find("badge")
            .val('')
            .end();

        EDITMODE = false;
        addClass(".badge", "invisible");
      });

    $("#btnLogin").click(function (){
        $.post("backend/login.php", {
                    username: $("#txtUsername").val(),   
                    password: $("#txtPassword").val()
                }, function (data, status) {
                    if (data == "ok") {
                        // data.redirect contains the string URL to redirect to
                        window.location.href = "../web-app/index.php";
                    }else
                    {
                        alert(data);
                    }
                });
    });

    $("#navSignOut").click(function (){
        $.post("backend/login.php", {
                    logout: "1"
                }, function (data, status) {
                    window.location.href = "../web-app/login.php";
                });
    });
});

function showModalError(msg)
{
    $("#modalError").text(msg);
    $("#modalError").removeClass("invisible");
}

function hideModalError()
{
    addClass("#modalError", "invisible");
}

function addClass(elem, cl)
{
    if(!$(elem).hasClass(cl)){
        $(elem).addClass(cl);
    }
}