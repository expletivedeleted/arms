<nav class="navbar navbar-expand-md navbar-dark bg-dark" style="margin-bottom:12px">
    <a class="navbar-brand mb-0 h1" href="#">Attendance and Room Management System</a>
    <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <?php 
        if(isset($_SESSION['login'])){
            echo ('<div class="collapse navbar-collapse" id="collapsibleNavId">
                <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
                <ul class="navbar-nav">

                    <li class="nav-item active">
                        <a class="nav-link" href="#" id="navAttendance">Attendance</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="navReservation">Reservation</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#" id="navPreferences">Preferences</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#" id="navSignOut">Sign Out</a>
                    </li>
                </ul>
                </div>
            </div>');
        }
    ?>
</nav>