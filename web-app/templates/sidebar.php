<div class="col-3 affix">

    <div class="list-group">
        <a href="index.php" class="list-group-item list-group-item-action <?php if ($_GET["tab"] == "") echo 'active'; ?> " id="sbSummary">
        <i class="fa fa-book"></i>
            Summary
        </a>
        
        <a href="rooms.php" class="list-group-item list-group-item-action <?php if ($_GET["tab"] == "rooms") echo 'active'; ?> " id="sbRooms">
        <i class="fa fa-user"></i>
            Rooms
        </a>

        <a href="users.php" class="list-group-item list-group-item-action <?php if ($_GET["tab"] == "users") echo 'active'; ?> " id="sbUserAccounts">
        <i class="fa fa-line-chart"></i>
            Faculty Accounts
        </a>
    </div>
</div>